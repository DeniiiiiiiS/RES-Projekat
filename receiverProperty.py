class ReceiverProperty:
    def __init__(self, code, receiver_value):
        self.code = code
        self.receiver_value = receiver_value

    def getCode(self):
        return self.code

    def getValue(self):
        return self.receiver_value
